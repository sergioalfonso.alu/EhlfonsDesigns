# Usa una imagen base de Apache
FROM httpd:2.4

# Copia los archivos y carpetas necesarios al directorio de trabajo en el contenedor
COPY css /usr/local/apache2/htdocs/css
COPY dtd /usr/local/apache2/htdocs/dtd
COPY fonts /usr/local/apache2/htdocs/fonts
COPY html /usr/local/apache2/htdocs/html
COPY media /usr/local/apache2/htdocs/media
COPY xml /usr/local/apache2/htdocs/xml
COPY xsl /usr/local/apache2/htdocs/xsl

# Expone el puerto 81 para que el servidor Apache pueda ser accesible desde el exterior
EXPOSE 81

# Establece el ServerName
RUN echo "ServerName localhost" >> /usr/local/apache2/conf/httpd.conf

# Inicia Apache en el primer plano cuando se ejecute el contenedor
CMD ["httpd", "-D", "FOREGROUND"]
